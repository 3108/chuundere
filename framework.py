from util import *

class IRCBot():
    def __init__(self,*args,**kwargs):
        self.config=defaultdict(list)
        self.resources=defaultdict(dict)
        self.message=defaultdict(dict)
        self.uptime=0

        self.socket=socket.socket()
        self.receivedbuffer=bytes()
        self.state_description=list()
        self.message_queue=list()
        
        atexit.register(self.cleanup)    

        self.config['default_mandatory_args']=['server','port','nick']
        self.config['messages_per_second']=2

        self.config.update(kwargs)
        self.open_file_list('configfile',self.read_config)
        # kwargs is reread so command line arguments would override ones in config files
        self.config.update(kwargs)

        self.open_file_list('resourcefile',self.read_resource)

        self.config['mandatory_args']+=self.config['default_mandatory_args']
        for conf_item in self.config['mandatory_args']:
            if is_unset(self.config[conf_item]):
                self.log('EE Mandatory configuration item "{conf_item}" unset!'.format(**locals()))
                exit()
        self.logfile_handle=None
        if not is_unset(self.config['logfile']):
            try:
                self.logfile_handle=open(self.config['logfile'])
            except OSError:
                self.log('WW Unable to open logfile {}'.format(self.config['logfile']))
                pass

        self.copy_if_unset('nick','ident','hostname','servername','realname')
        pprint(self.config)
        self.connect()
        self.listen()

    def listen(self):
        while True:
            self.receive()
            self.parse_buffer()
            for self.message['current_line'] in self.lines:
                self.log(self.message['current_line'])
                self.parse_line()
                self.describe_state()
                self.do_when_commands()
                self.message.clear()
            self.message['uptime']=self.uptime
            self.describe_state()
            self.do_when_commands()
            self.message.clear()
            self.send_message_queue()
            time.sleep(1)
            self.uptime+=1

    def parse_line(self):
        current_line=self.message['current_line']
        nick,text=[[] for i in range(2)]
        irc_command=current_line
        if current_line.startswith(':'):
            nick,irc_command=current_line.partition(' ')[::2]
        if ':' in irc_command:
            irc_command,text=irc_command.partition(':')[::2]
        irc_command,irc_args=irc_command.partition(' ')[::2]
        irc_args=irc_args.split(' ')[:-1]
        if '@' in nick:
            nick,hostname=nick.partition('@')[::2]
        if '!' in nick:
            nick,realname=nick.partition('!')[::2]
        if irc_command=='PRIVMSG':
            if text.startswith(self.config['command_marker']):
                command=text[len(self.config['command_marker']):]
                arguments=command.split(' ')
                command=arguments[0]
            if irc_args[0].startswith('#'):
                reply_to=irc_args[0]
            else:
                reply_to=nick
        to_add=locals()
        del to_add['self']
        self.message.update(to_add)

    def send_message_queue(self):
        if is_unset(self.config['messages_per_second']):
            while self.message_queue:
                self.send_raw_msg(self.message_queue.pop(0))
        else:
            for i in range(self.config['messages_per_second']):
                if self.message_queue:
                    self.send_raw_msg(self.message_queue.pop(0))

    def describe_state(self):
        self.state_description.clear()
        for i in ['irc_command','uptime','command']:
            if not is_unset(self.message[i]):
                self.state_description.append('_'.join(['when',i,str(self.message[i])]))
            else: 
                del self.message[i]

    def do_when_commands(self):
        for method_name in self.state_description:
            if method_name in dir(self) and callable(getattr(self,method_name)):
                getattr(self,method_name)()                
            elif method_name.startswith('when_command_'):
                self.when_unrecognized_command()

    def connect(self):
        self.socket.connect((self.config['server'],self.config['port']))
        self.socket.setblocking(False)

    def receive(self):
        try:
            while(1):
                self.receivedbuffer+=self.socket.recv(4096)
        except BlockingIOError:
            pass

    def parse_buffer(self):
        self.lines=self.receivedbuffer.decode().split('\r\n')[:-1]
        self.receivedbuffer=b''

    def when_uptime_2(self):
        self.send_raw_msg(" ".join(["NICK", self.config['nick']]))
        self.send_raw_msg(" ".join(["USER", self.config['ident'], self.config['hostname'],
            self.config['servername'], ':', self.config['realname']]))

    def when_uptime_4(self):
        if not is_unset(self.config['nickserv_password']):
           self.queue_privmsg('NICKSERV','IDENTIFY '+self.config['nickserv_password']) 

    def when_uptime_6(self):
        for i in self.config['channels']:
            self.send_raw_msg('JOIN '+i)

    def when_irc_command_PING(self):
        self.send_raw_msg('PONG :'+self.message['text'])

    def when_command_help(self):
        '''Built-in help system'''
        if not self.config['disable_builtin_help']:
            if not self.message['arguments'][1:]:
                cmd_list=[i.partition('when_command_')[2] for i in dir(self) if i.startswith('when_command_')]
                cmd_str=self.config['command_marker']+(', '+self.config['command_marker']).join(cmd_list)
                self.reply('Defined commands: '+cmd_str+\
                '\nMessage `{}help <command>` for further info (if available)'.format(self.config['command_marker']))
            else:
                method_name='when_command_'+self.message['arguments'][1]
                if method_name in dir(self):
                    if getattr(self,method_name).__doc__:
                        self.reply(self.message['arguments'][1]+': '+getattr(self,method_name).__doc__)
                    else:
                        self.reply('No further information is available')
                else:
                    self.reply('No such command defined')
                
        else:
            self.reply('Built-in help disabled.')

    def when_unrecognized_command(self):
        if not self.config['disable_builtin_help']:
            self.reply('Unrecognized command. Message `{}help` for help'.format(self.config['command_marker']))
        else:
            self.reply('Unrecognized command.')

    def reply(self,msg):
        self.queue_privmsg(self.message['reply_to'],msg)

    def reply_notice(self,msg):
        self.queue_notice(self.message['reply_to'],msg)

    def reply_private(self,msg):
        self.queue_privmsg(self.message['nick'],msg)

    def reply_notice_private(self,msg):
        self.queue_notice(self.message['nick'],msg)

    def send_raw_msg(self,msg):
        self.socket.sendall(bytes(html.unescape(msg)+'\r\n' ,"UTF-8"))
        self.log('<<',msg.encode("ascii","xmlcharrefreplace").decode())

    def queue_privmsg(self,sendto,message):
        messages=[]
        for i in message.split('\n'):
            messages.extend(i[j*400:(j+1)*400] for j in range(int(len(i)/400)+1) )
        for i in messages:
            self.message_queue.append('PRIVMSG '+sendto+' :'+i)
        
    def queue_notice(self,sendto,message):
        messages=[]
        for i in message.split('\n'):
            messages.extend(i[j*400:(j+1)*400] for j in range(int(len(i)/400)+1) )
        for i in messages:
            self.message_queue.append('NOTICE '+sendto+' :'+i)

    def copy_if_unset(self,*args):
        for conf_item in args[1:]:
            self.config[conf_item]=self.config[args[0]]

    def log(self,*args):
        if not is_unset(self.config['only_log_when']):
            for i in self.config['only_log_when']:
                if i in self.state_description:
                    print(*args)
                    if self.logfile_handle:
                        print(*args,file=self.logfile_handle)
                    return
            return
        if not is_unset(self.config['dont_log_when']):
            for i in self.config['dont_log_when']:
                if i in self.state_description:
                    return
        print(*args)
        if self.logfile_handle:
            print(*args,file=self.logfile_handle)

    def read_resource(self,file_descriptor):
        resource_name=get_basename(file_descriptor.name)
        self.resources[resource_name]=json.load(file_descriptor)

    def write_resource(self,file_descriptor):
        resource_name=get_basename(file_descriptor.name)
        json.dump(self.resources[resource_name],file_descriptor)

    def read_config(self,file_descriptor):
        self.config.update(json.load(file_descriptor))

    def cleanup(self):
        self.open_file_list('resourcefile',self.write_resource,'w')

    def open_file_list(self,role,method,mode='r'):
        if is_list(self.config[role]):
            for filename in self.config[role]:
                self.open_file(method,filename,mode)
        elif is_unset(self.config[role]):
            return
        else:
            self.open_file(method,self.config[role],mode)

    def open_file(self,method,filename,mode):
        try:
            file_descriptor=open(os.path.expanduser(filename),mode)
            method(file_descriptor)
            file_descriptor.close()
        except FileNotFoundError:
            self.log('WW Unable to open "{filename}" !'.format(**locals()))

if __name__=="__main__":
    IRCBot(**parse_arguments(sys.argv[1:]))
