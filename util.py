import sys, json, os, atexit, socket, time, html
from pprint import pprint
from collections import defaultdict

def is_unset(obj):
    """ Returns true if configuration iten is unset (is a bool or evaluates as false) """
    return not obj or isinstance(obj,bool)

def is_list(obj):
    """ Returns True if obj is iterable and not a string"""
    return hasattr(obj,'__iter__') and not isinstance(obj,str)

def get_basename(path):
    """ Returns file name without path to it and it's extension """
    return os.path.splitext(os.path.basename(os.path.expanduser(path)))[0]

def parse_object(obj):
    """ Returns obj as object, deserialized with JSON decoder, or as string if that fails"""
    try:
        return json.loads(obj)    
    except json.decoder.JSONDecodeError:
        return obj

def parse_arguments(argv):
    """ Returns a dictionary of argument names mapped to their values
        if no value follows argument, --<name> maps <name> to True
        Unnamed values are presumed to be config files"""
    result_dict={}
    argument_name="configfile"
    for i in argv:
        if i.startswith('--'):
            argument_name=i[2:]
            result_dict[argument_name]=True
        else:
            result_dict[argument_name]=parse_object(i)
            argument_name="configfile"
    return result_dict

